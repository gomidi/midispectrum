package main

import (
	"fmt"
	"os"

	"gitlab.com/gomidi/midispectrum"
	"gitlab.com/metakeule/config"
)

var (
	cfg    = config.MustNew("midispectrum", "0.0.1", "paint a MIDI spectrum")
	inArg  = cfg.NewString("in", "SMF file", config.Required)
	outArg = cfg.NewString("out", "PNG file", config.Required)
)

func main() {
	err := run()

	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %v\n", err)
		os.Exit(1)
	}

	os.Exit(0)
}

func run() error {
	err := cfg.Run()

	if err != nil {
		fmt.Fprintf(os.Stdout, cfg.Usage())
		return err
	}

	return midispectrum.MakeImage(inArg.Get(), outArg.Get())
}
