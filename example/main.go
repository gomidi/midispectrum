package main

import (
	"fmt"
	"os"

	"gitlab.com/gomidi/midi/mid"
)

func main() {
	err := run()

	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %v\n", err)
		os.Exit(1)
	}

	os.Exit(0)
}

func run() error {
	return mid.NewSMFFile("test.mid", 1, func(wr *mid.SMFWriter) error {
		for key := uint8(34); key < 119; key++ {
			wr.SetChannel(0)
			//wr.NoteOn(key, key-30)
			wr.NoteOn(key, 7)
			wr.SetDelta(wr.Ticks16th())
			wr.SetChannel(1)
			wr.Pitchbend(-2048)
			//wr.NoteOn(key+1, key+40)
			wr.NoteOn(key+1, 7)
			wr.SetChannel(0)
			wr.SetDelta(wr.Ticks4th())
			wr.NoteOff(key)
			wr.SetChannel(1)
			wr.SetDelta(wr.Ticks16th())
			wr.NoteOff(key + 1)
			//wr.SetDelta(wr.Ticks16th())
			//			wr.EndOfTrack()
		}

		return nil
	})
}
