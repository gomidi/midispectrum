package midispectrum

import (
	"fmt"
	"image/color"
	"image/draw"
	"math"

	"github.com/fogleman/gg"
	"gitlab.com/gomidi/midi/mid"
)

/*

This package visualizes special crafted "spectrum" SMF files
with the following properties:

- each note may live on its own channel
- all channel messages (pitch bend, volume etc) on this channel would effect any note on this channel
- so there would be 16 times polyphony within one track where each note would have its own channel message
- in order for that to work, all channel messages must be reset after each note
- in addition multiple tracks can be used to get more polyphony
- pitchbend will have the default range of +-2 semitones. no other pitchbend range can be set (for now)
- polyaftertouch is used to set the loudness after the initial velocity value. the first polyaftertouch message
  is expected to match the velocity of the note
- the same MIDI note may only occure once at a time in the whole SMF file.

The "spectrum" SMF file will be rendered the following way:
a 1-bit transparent PNG file that is black and transparent only where the notes are.
The frequencies are on the x-axis (low->high = left->right) and the time is on the y-axis
(top->down = past->future).
On the y-axis 1px == 50ms, i.e. 20px == 1 sec <=> 1200px == 1 minute.
On the x-axis there is space for 168 frequencies (7 octaves @ 24 quarter tones from 58 Hz to 7459).
Each frequency has a width of 7 pixel: the total width is 1176px.
The placement of the transparent area is as follows:
On the time axis (y-axis) the start and end of the MIDI note falls into the next 50ms slot. The start is placed
on the 50ms slot and all time slots until but not including the end slot.
The placement on the x-axis happens by placing the reference point on the fractional MIDI-note that is calculated
by applying the current pitchbend of the corresponding MIDI channel on the same track.
The thickness of the rectangle is determined by the velocity (on MIDI-on) or the polyaftertouch (on running MIDI-notes)
value. Polyaftertouch for not running notes are ignored. The thickness is calculated as follows:

velocity/pat  breite       positionierung (o -> referenz)
1-19          1px                 o
20-37         2px                 ox
38-55         3px                xox
56-73         4px                xoxx
74-91         5px               xxoxx
92-109        6px               xxoxxx
110-127       7px              xxxoxxx

A five minute piece would have 5*1200*1176 = 7056000px @ 1bit = 861.33Kb
In addition there is a background file that has the colors of the frequencies.
That file has a height of 1px and a width of 1176px = 1176px @ 24bit = 3.45Kb.
In a browser it can be repeated as background-image top to down.
There might be a legend for the frequencies/note names that would be like 50px
high and 1176px wide = 58800px @ 1 bit = 7.18Kb

There could be a webbrowser mode that shows the files layered properly and
allows scrolling through the time axis (top-down) as well as have a vertical overview
(zoomed out/scaled down) and a vertical split view to compare parts at different times.
A kind of vertical zoom out would be nice as having annotations and collapsable time
areas on the left side.


*/

//const maxHeight = 262144 // stolen from Gimp, probably still too large

const maxHeight = 65536
const minNote = 34

//const maxNote = 118
const maxNote = 108
const width = ((maxNote-minNote)*2 + 1) * 7

var barChanges = map[int][2]uint8{}
var tempoChanges = map[int]float64{}

func MakeImage(smffile, imageFile string) error {
//	fmt.Printf("reading %#v...", smffile)
	err := ReadSMF(smffile)
	if err != nil {
		return err
	}

	if lastLine > maxHeight {
		fmt.Printf("done\nimage too long, truncating at %d (%0.2f hours)...", maxHeight, float64(maxHeight)/float64(60*60))
		lastLine = maxHeight
	}

	/*
		fmt.Printf("done\ncreating background image %dx%d...", width, lastLine)

		err = makeBackgroundImage(lastLine)

		if err != nil {
			return err
		}
	*/

	//fmt.Printf("done\ncreating image %dx%d", width, lastLine)

	// 942500
	// 262144 max size of gimp
	// 2,147,483,647
	// 1 108 380 000
	// 262144 by 262144

	//img := newPaletteImage(1176 /* width */, lastLine /* height */, color.Black)
	img := newForegroundImage(width /* width */, lastLine /* height */)

	mkFreqGrid(img, lastLine)

	//	setDimensions(1176 /* width */, lastLine /* height */ )
	// set all black
	var currentNotes [128]uint8
	var currentPitches [128]int16
	var lastTempo float64 = 120
	var lastTimeSig = [2]uint8{4, 4}
	var lastBar int = 0
	_ = lastTimeSig

	isNewBar := func(ln int) bool {
		//beatsPerBar := float64(lastTimeSig[0]) * 4.0 / float64(lastTimeSig[1])
		beatsPerBar := float64(4.0)
		//fac := float64(1 / 4.0)
		//(lastTempo / 60) / fac

		barDurMS := 60000000 * beatsPerBar / math.Round(lastTempo*1000)
		//		barDurMS := beatDurMS
		// 2666.668 MS
		barDurMS60 := int(math.Round(barDurMS / 60))

        _ = barDurMS60
		//		barLengthMS50 := int(math.Round(fac * lastTempo * 60 / 1000 * 20))
		//fmt.Printf("lastTempo: %v\n", math.Round(lastTempo*1000))
		//		fmt.Printf("beatDurMS: %v\n", beatDurMS)
		//fmt.Printf("barDurMS: %0.5f\n", math.Round(barDurMS))
		//fmt.Printf("barDurMS60: %v\n", barDurMS60)
		//		fmt.Printf("div: %0.4f\n", (float64(ln-lastBar)*20)/barDurMS)
		//		fmt.Printf("rem: %v\n", ((ln-lastBar)*20)%int(math.Round(barDurMS)))
		//		is := (((ln - lastBar) * 20) % int(math.Round(barDurMS))) < 20
		//fmt.Printf("diff: %0.5f\n", float64(ln-lastBar)*60-barDurMS)
		is := math.Abs(barDurMS-float64(ln-lastBar)*60) < 60
		//is := (ln-lastBar)%barDurMS60 == 0
		//fmt.Printf("is barchange: %v\n", is)
		return is
	}

	for lineNo := 0; lineNo < lastLine; lineNo++ {

		if isNewBar(lineNo) {
			//fmt.Printf("bar change at %d\n", lineNo)
			lastBar = lineNo

			for _x := 0; _x <= width; _x++ {
				Set(img, _x, lineNo, DarkGrey)
			}
		}

		if tc, has := tempoChanges[lineNo]; has {
			lastTempo = tc
			_ = tc
		}

		if bc, has := barChanges[lineNo]; has {
			lastTimeSig = bc
		}

		line, has := cache[lineNo]
		/*
		if lineNo%6000 == 0 {
			fmt.Print(".")
		}
		*/

		for key, vals := range line {
			if has && key >= minNote && key < maxNote+1 {
				switch {
				case vals[0] == 0:
					currentNotes[key] = 0
					currentPitches[key] = 0
					//					fmt.Printf("stop %d\n", key)
				case vals[0] > 0:
					//					fmt.Printf("start %d (%d)\n", key, uint8(vals[0]))
					currentNotes[key] = uint8(vals[0])
					currentPitches[key] = vals[1]
				default:
					panic("must not happen")
				}
			}
		}
		var currentLine = [width]bool{}

		for key := minNote; key < maxNote+1; key++ {
			vel := currentNotes[key]
			if vel == 0 {
				continue
			}
			relKey := int(key-minNote) * 2 // quartertones
			// +-8192 == +-2 semitones == +-4 quartertones
			if currentPitches[key] != 0 {
				pitchDiff := int(math.Round(float64(currentPitches[key]) / float64(2048)))
				//				fmt.Printf("pitchDiff: %v\n", pitchDiff)
				relKey += pitchDiff
			}

			refPoint := int(relKey)*7 + 3 /* center of 7px width freq band */
			//			fmt.Printf("key: %d relKey: %d refPoint: %d\n", key, relKey, refPoint)

			switch {
			case vel < 20: // 1px
				currentLine[refPoint] = true
			//case vel < 38: // 2px
			case vel < 48: // 2px
				currentLine[refPoint] = true
				currentLine[refPoint+1] = true
			//case vel < 56: // 3px
			case vel < 69: // 3px
				currentLine[refPoint-1] = true
				currentLine[refPoint] = true
				currentLine[refPoint+1] = true
				//			case vel < 74: // 4px
			case vel < 90: // 4px
				currentLine[refPoint-1] = true
				currentLine[refPoint] = true
				currentLine[refPoint+1] = true
				currentLine[refPoint+2] = true
			//			case vel < 92: // 5px
			case vel < 108: // 5px
				currentLine[refPoint-2] = true
				currentLine[refPoint-1] = true
				currentLine[refPoint] = true
				currentLine[refPoint+1] = true
				currentLine[refPoint+2] = true
			//case vel < 110: // 6px
			case vel < 120: // 6px
				currentLine[refPoint-2] = true
				currentLine[refPoint-1] = true
				currentLine[refPoint] = true
				currentLine[refPoint+1] = true
				currentLine[refPoint+2] = true
				currentLine[refPoint+3] = true
			default: // 7px
				currentLine[refPoint-3] = true
				currentLine[refPoint-2] = true
				currentLine[refPoint-1] = true
				currentLine[refPoint] = true
				currentLine[refPoint+1] = true
				currentLine[refPoint+2] = true
				currentLine[refPoint+3] = true
			}
		}

		for _x, isSet := range currentLine {
			//			var cl color.Color = color.Black
			if isSet {
				//				cl = color.Transparent
				//			img.Set(_x, lineNo, cl)
				//img.Set(_x, lineNo, color.Transparent)
				Set(img, _x, lineNo, color.Transparent)
			}
		}
	}
	bgImg := mkBgImage(lastLine)
	mergeFrontToBack(bgImg, img)

//	fmt.Printf("done\nwriting image...")

	//return gg.SavePNG(imageFile, img)
	return gg.SavePNG(imageFile, bgImg)
}

//var cache = make([][127][2]int16, 1200 /* 1 minute piece */) // velocity/polyaftertouch and pitchbend; velocity == -1 means: noteoff
var cache = map[int]map[int][2]int16{}

var lastLine int

//var

func isNoteRunning(posMs, key int) bool {
	for pos := posMs; pos >= 0; pos-- {
		data, has := cache[pos]
		if has {
			if vals, hasVals := data[key]; hasVals {
				return vals[0] > 0
			}
			//			fmt.Printf("key: %d\n", key)
		}
	}

	// we are at the beginning: there was never a noteon
	return false
}

// velocity <= 0 means: noteoff
func setVelocity(posMs, key int, velocity uint8) {
	if velocity == 0 && !isNoteRunning(posMs, key) {
		return
	}

	if velocity != 0 && isNoteRunning(posMs, key) {
		return
	}
	if posMs > lastLine {
		lastLine = posMs
	}

	data, has := cache[posMs]
	if !has {
		data = map[int][2]int16{}
	}

	if _, hasKey := data[key]; !hasKey {
		data[key] = [2]int16{}
	}
	data[key] = [2]int16{int16(velocity), data[key][1]}
	cache[posMs] = data
}

func setPolyAftertouch(posMs, key int, value uint8) {
	if !isNoteRunning(posMs, key) {
		return
	}
	if posMs > lastLine {
		lastLine = posMs
	}

	data, has := cache[posMs]
	if !has {
		data = map[int][2]int16{}
	}

	if _, hasKey := data[key]; !hasKey {
		data[key] = [2]int16{}
	}
	data[key] = [2]int16{int16(value), data[key][1]}
	cache[posMs] = data
}

func setPitch(posMs, key int, pitchbend int16) {
	if !isNoteRunning(posMs, key) {
		return
	}
	if posMs > lastLine {
		lastLine = posMs
	}

	data, has := cache[posMs]
	if !has {
		data = map[int][2]int16{}
	}

	if _, hasKey := data[key]; !hasKey {
		data[key] = [2]int16{}
	}
	data[key] = [2]int16{data[key][0], pitchbend}
	cache[posMs] = data
}

func msSlot(pos mid.Position, rd *mid.Reader) int {
	dur := rd.TimeAt(pos.AbsoluteTicks)
	//	fmt.Printf("")
	//return int(dur.Nanoseconds() / 50000000)
	return int(dur.Nanoseconds() / 60000000)
}

func mkBgImage(height int) draw.Image {
	img := newBackgroundImage(width, height)

	for nt := 0; nt < ((maxNote-minNote)*2)+1; nt++ {
		cl := getColorForNote(uint8(nt))

		refPoint := nt*7 + 3

		for _x := refPoint - 3; _x < refPoint+4; _x++ {
			for _y := 0; _y <= height; _y++ {
				//img.Set(_x, _y, cl)
				Set(img, _x, _y, cl)
			}
		}
	}
	return img
}

func mkFreqGrid(img draw.Image, height int) {
	for nt := 0; nt < ((maxNote-minNote)*2)+1; nt++ {
		if (nt+1)%2 == 0 {
			continue
		}
		refPoint := nt*7 + 3

		for _y := 0; _y <= height; _y++ {
			//img.Set(_x, _y, cl)
			Set(img, refPoint, _y, DarkGrey)
		}

	}
}

func makeBackgroundImage(height int) error {
	return gg.SavePNG("background.png", mkBgImage(height))
}

func ReadSMF(file string) error {
	var (
		channelPitchBend [16]int16
		channelNotes     [16]uint8
		rd               = mid.NewReader(mid.NoLogger())
	)

	barChanges[0] = [2]uint8{4, 4}
	tempoChanges[0] = 120

	rd.Msg.Meta.TempoBPM = func(pos mid.Position, bpm float64) {
		ms := msSlot(pos, rd)
		tempoChanges[ms] = bpm
	}

	rd.Msg.Meta.TimeSig = func(pos mid.Position, num, denom uint8) {
		ms := msSlot(pos, rd)
		barChanges[ms] = [2]uint8{num, denom}
	}

	rd.Msg.Channel.Pitchbend = func(pos *mid.Position, channel uint8, pitchbend int16) {
		if key := channelNotes[channel]; key > 0 {
			setPitch(msSlot(*pos, rd), int(key), pitchbend)
		}
		channelPitchBend[channel] = pitchbend
	}

	rd.Msg.Channel.NoteOn = func(pos *mid.Position, channel, key, velocity uint8) {
		channelNotes[channel] = key
		ms := msSlot(*pos, rd)
		setVelocity(ms, int(key), velocity)
		setPitch(ms, int(key), channelPitchBend[channel])
	}

	rd.Msg.Channel.NoteOff = func(pos *mid.Position, channel, key, velocity uint8) {
		channelNotes[channel] = 0
		ms := msSlot(*pos, rd)
		setVelocity(ms, int(key), 0)
	}

	rd.Msg.Channel.PolyAftertouch = func(pos *mid.Position, channel, key, pressure uint8) {
		ms := msSlot(*pos, rd)
		setPolyAftertouch(ms, int(key), pressure)
		setPitch(ms, int(key), channelPitchBend[channel])
	}

	rd.Msg.Meta.EndOfTrack = func(pos mid.Position) {
		channelPitchBend = [16]int16{}
		channelNotes = [16]uint8{}
	}

	return rd.ReadSMFFile(file)
}
