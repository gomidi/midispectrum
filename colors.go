package midispectrum

import (
	"image"
	"image/color"
)

type palette struct {
	color.Palette
}

var (
	LightGrey = color.RGBA{187, 187, 187, 255}
	//DarkGrey  = color.RGBA{120, 120, 120, 255}
	DarkGrey = color.RGBA{30, 30, 30, 255}
)

var ColorPalette = &palette{color.Palette{
	color.Black,
	color.Transparent,
	//color.White,
	DarkGrey,
}}

var BackgroundPalette = &palette{color.Palette{
	color.Black,
	color.Transparent,
	DarkGrey,
	//	color.White,
	ColorYellow,
	ColorElectricLime,
	ColorSpringBud,
	ColorHarlequin,
	ColorMalachite,
	ColorCaribbeanGreen,
	ColorRobinsEggBlue,
	ColorDeepSkyBlue,
	ColorDodgerBlue,
	ColorNavyBlue,
	ColorBlue,
	ColorDarkBlue,
	ColorIndigoBlue,
	ColorElectricIndigo,
	ColorDarkViolet,
	ColorDeepMagenta,
	ColorRazzmatazz,
	ColorRed,
	ColorOrangeRed,
	ColorSafetyOrange,
	ColorDarkOrange,
	ColorSelectiveYellow,
	ColorTangerineYellow,
	ColorGoldenYellow,
}}

func (p *palette) Quantize(pl color.Palette, im image.Image) color.Palette {
	return pl
}

// the given note here is a quarternote
func getColorForNote(note uint8) color.Color {
	return bgColorMapper[((note+(2*minNote))-24)%24]
}

var bgColorMapper = [24]color.Color{
	ColorYellow,
	ColorElectricLime,
	ColorSpringBud,
	ColorHarlequin,
	ColorMalachite,
	ColorCaribbeanGreen,
	ColorRobinsEggBlue,
	ColorDeepSkyBlue,
	ColorDodgerBlue,
	ColorNavyBlue,
	ColorBlue,
	ColorDarkBlue,
	ColorIndigoBlue,
	ColorElectricIndigo,
	ColorDarkViolet,
	ColorDeepMagenta,
	ColorRazzmatazz,
	ColorRed,
	ColorOrangeRed,
	ColorSafetyOrange,
	ColorDarkOrange,
	ColorSelectiveYellow,
	ColorTangerineYellow,
	ColorGoldenYellow,
}

var (
	ColorYellow          = color.RGBA{255, 255, 0, 255} // yellow
	ColorElectricLime    = color.RGBA{201, 255, 0, 255} // yellow-green
	ColorSpringBud       = color.RGBA{148, 255, 0, 255} // light green
	ColorHarlequin       = color.RGBA{81, 255, 0, 255}  // green
	ColorMalachite       = color.RGBA{0, 232, 83, 255}  // dark green
	ColorCaribbeanGreen  = color.RGBA{0, 206, 133, 255} // turkis
	ColorRobinsEggBlue   = color.RGBA{0, 196, 190, 255} // blue-green
	ColorDeepSkyBlue     = color.RGBA{0, 173, 236, 255} // sky blue
	ColorDodgerBlue      = color.RGBA{0, 130, 255, 255} // light blue
	ColorNavyBlue        = color.RGBA{0, 83, 255, 255}  // deep blue
	ColorBlue            = color.RGBA{0, 57, 255, 255}  // blue
	ColorDarkBlue        = color.RGBA{0, 0, 255, 255}   // dark blue
	ColorIndigoBlue      = color.RGBA{90, 0, 252, 255}  // indigo blue
	ColorElectricIndigo  = color.RGBA{105, 0, 210, 255} // indigo
	ColorDarkViolet      = color.RGBA{153, 0, 202, 255} // violet
	ColorDeepMagenta     = color.RGBA{201, 0, 195, 255} // magenta
	ColorRazzmatazz      = color.RGBA{247, 0, 117, 255} // pink
	ColorRed             = color.RGBA{255, 0, 0, 255}   // red
	ColorOrangeRed       = color.RGBA{255, 60, 0, 255}  // orange red
	ColorSafetyOrange    = color.RGBA{255, 106, 0, 255} // orange
	ColorDarkOrange      = color.RGBA{255, 149, 0, 255} // dark orange
	ColorSelectiveYellow = color.RGBA{255, 176, 0, 255} // yellow-orange
	ColorTangerineYellow = color.RGBA{255, 202, 0, 255} // dark yellow
	ColorGoldenYellow    = color.RGBA{255, 227, 0, 255} // deep yellow

	//                                                                             names according to https://www.htmlcsscolor.com
	PrimeColor = ColorYellow //                               C  hellgelb 255 255   0   ffff00 Yellow
	//                                                               201 255   0   c9ff00 Electric Lime
	MinSecondColor = ColorSpringBud //                          C# hellgrün 148 255   0   94ff00 Spring Bud
	//                                                                81 255   0   51ff00 Harlequin
	MajSecondColor = ColorMalachite //                             D  dunkelgrün 0 232  83   00e853 Malachite
	//                                                                 0 206 133   00ce85 Caribbean Green
	MinThirdColor = ColorRobinsEggBlue // oliv                         D#  türkis    0 196 190   00c4be Robin's Egg Blue
	//                                                                 0 173 236   00adec Deep Sky Blue
	MajThirdColor = ColorDodgerBlue // türkis                E hellblau    0 130 255   0082ff Dodger Blue
	//                                                                 0  83 255   0053ff Navy Blue
	FourthColor = ColorBlue // activ red                       F dunkelblau  0  57 255   0039ff Blue
	//                                                                 0   0 255   0000ff Blue
	TritoneColor = ColorIndigoBlue //                               F# violet    90   0 252   5a00fc Electric Indigo
	//                                                               105   0 210   6900d2 Electric Indigo
	FifthColor = ColorDarkViolet // activ blue                   G  purple   153   0 202   9900ca Dark Violet
	//                                                               201   0 195   c900c3 Deep Magenta
	MinSixthColor = ColorRazzmatazz // burgunder rot-brown       G#  pink    247   0 117   f70075 Razzmatazz
	//                                                               255   0   0   ff0000 Red
	MajSixthColor = ColorOrangeRed //                            A      rot  255  60   0   ff3c00 Orange Red
	//                                                               255 106   0   ff6a00 Safety Orange
	MinSeventhColor = ColorDarkOrange // dark/deep blue        A#  orange  255 149   0   ff9500 Dark Orange
	//                                                               255 176   0   ffb000 Selective Yellow
	MajSeventhColor = ColorTangerineYellow //                         H dunkelgelb255 202   0   ffca00 Tangerine Yellow
	//                                                               255 227   0   ffe300 golden yellow
	//                                                   C hellgelb  255 255   0   ffff00 yellow
)
