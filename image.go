package midispectrum

import (
	"image"
	"image/color"
	"image/draw"
)

func Set(img draw.Image, _x, _y int, cl color.Color) {
	//img.Set(_x, _y, cl)
	img.Set(_y, width-_x, cl)
}

func newForegroundImage(xmax, ymax int) draw.Image {
	//img := image.NewPaletted(image.Rect(0, 0, xmax, ymax), ColorPalette.Palette)
	img := image.NewPaletted(image.Rect(0, 0, ymax, xmax), ColorPalette.Palette)
	draw.Draw(img, img.Bounds(), &image.Uniform{color.Black}, image.ZP, draw.Src)
	return img
}

func mergeFrontToBack(bg draw.Image, fg draw.Image) {
	draw.Draw(bg, fg.Bounds(), fg, image.ZP, draw.Over)
}

func newBackgroundImage(xmax, ymax int) draw.Image {
	return image.NewPaletted(image.Rect(0, 0, ymax, xmax), BackgroundPalette.Palette)
	//	return image.NewPaletted(image.Rect(0, 0, xmax, ymax), BackgroundPalette.Palette)
}

func newImage(xmax, ymax int, background color.Color) draw.Image {
	//img := image.NewRGBA(image.Rect(0, 0, xmax, ymax))
	img := image.NewRGBA(image.Rect(0, 0, ymax, xmax))

	if background != nil {
		for _x := 0; _x < xmax; _x++ {
			for _y := 0; _y < ymax; _y++ {
				img.Set(_x, _y, background)
			}
		}
	}

	return img
}
