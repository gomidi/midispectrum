module gitlab.com/gomidi/midispectrum

go 1.14

require (
	github.com/fogleman/gg v1.1.0
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	gitlab.com/gomidi/midi v1.15.3
	gitlab.com/metakeule/config v1.13.0
	golang.org/x/image v0.0.0-20181116024801-cd38e8056d9b // indirect
)
